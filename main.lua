local MetaBallManager = require("objects/MetaBallManager")

function love.load()
  WIDTH, HEIGHT = love.graphics.getDimensions()

  love.graphics.setDefaultFilter("nearest")
  love.graphics.setBackgroundColor(1,0.7,0.2)
  canvas = love.graphics.newCanvas(WIDTH, HEIGHT)
  metaballManager = MetaBallManager:new()
end

function love.update(dt)
  local mx, my  = love.mouse.getPosition()

  if love.mouse.isDown(1) then
    metaballManager:addMetaBall(mx, my)
  end
  metaballManager:update(dt)
end

function love.draw()
  metaballManager:draw(canvas)
end
