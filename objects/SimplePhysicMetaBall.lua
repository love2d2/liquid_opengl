local MetaBall = require("objects/MetaBall")

local SimplePhysicMetaBall = {}
SimplePhysicMetaBall.__index = SimplePhysicMetaBall

-- setup inheritance
setmetatable(SimplePhysicMetaBall, {__index = MetaBall})

function SimplePhysicMetaBall:new(pX, pY, pVx, pVy)
  local self = MetaBall:new(pX, pY, "assets/water.png")
  self.vx = pVx
  self.vy = pVy

  setmetatable(self, SimplePhysicMetaBall)
  return self
end

function SimplePhysicMetaBall:update(dt)
  if self.x > WIDTH then 
    self.vx = 0
    self.x = WIDTH 
  end
  if self.x < 0 then 
    self.vx = 0
    self.x = 0
  end
  if self.y > HEIGHT then
      self.vy = 0
      self.y = HEIGHT
  end
  if self.y < 0 then
    self.vy = 0
    self.y = 0
  end

  if self.vx > 0 then
    if  self.vx < 0 then
      self.vx = 0
    else
      self.vx = self.vx - 1
    end
  end

  if self.vx < 0 then
    if  self.vx > 0 then
      self.vx = 0
    else
      self.vx = self.vx + 1
    end
  end

  self.vy = self.vy + 12
  self.x = self.x + self.vx * dt
  self.y = self.y + self.vy * dt
end

return SimplePhysicMetaBall
